import testinfra

def test_config_dir(File):
    file = File('/etc/alertmanager')
    assert file.is_directory
    assert file.user == 'root'

def test_config_dir(File):
    file = File('/etc/alertmanager/templates')
    assert file.is_directory
    assert file.user == 'root'


def test_config_file_exists_and_owner(File):
    file = File('/etc/alertmanager/alertmanager.yml')
    assert file.is_file
    assert file.user == 'root'

def test_service_is_running_and_enabled(Service):
    alertmanager = Service('alertmanager')
    assert alertmanager.is_running

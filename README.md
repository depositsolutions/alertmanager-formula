# alertmanager-formula

A saltstack formula created to setup an [Alertmanager server](https://github.com/prometheus/alertmanager) for [Prometheus](https://www.prometheus.io).

# Known problems
 - the cluster functionality is only supported for alertmanager 0.15.0-rc.0
 - right now the formula only supports clustering on the default port 9094


# Available states

## init

The essential Alertmanager state running both ``install`` and ``config``.

## install


- Download alertmanager release from Github into the dist dir  (defaults to /opt/alertmanager/dist)
- link the binary (defaults to /usr/bin/alertmanager)
- Register the appropriate service definition with systemd.

This state can be called independently.


## config

Deploy the configuration files required to run the service, and enable the
service.

Configuration is done through the ``alertmanager`` pillar.

This state can be called independently but does not make an aweful lot of sense.

### sample pillar:
```yaml
alertmanager:
  bin_dir: '/usr/bin'
  dist_dir: '/opt/alertmanager/dist'
  config_dir: '/etc/alertmanager'
  data_dir: '/var/lib/alertmanager'
  version: '0.11.0'
  service: True
  service_user: 'alertmanager'
  service_group: 'alertmanager'
  config:
    global:
      # The smarthost and SMTP sender used for mail notifications.
      smtp_smarthost: 'localhost:25'
      smtp_from: 'alertmanager@example.org'
      smtp_auth_username: 'alertmanager'
      smtp_auth_password: 'some_password'
    # The directory from which notification templates are read.
    templates:
    - '/etc/alertmanager/template/*.tmpl'
    # The root route on which each incoming alert enters.
    route:
      # The labels by which incoming alerts are grouped together. For example,
      # multiple alerts coming in for cluster=A and alertname=LatencyHigh would
      # be batched into a single group.
      group_by: ['alertname', 'cluster', 'service']

      # When a new group of alerts is created by an incoming alert, wait at
      # least 'group_wait' to send the initial notification.
      # This way ensures that you get multiple alerts for the same group that start
      # firing shortly after another are batched together on the first
      # notification.
      group_wait: 30s

      # When the first notification was sent, wait 'group_interval' to send a batch
      # of new alerts that started firing for that group.
      group_interval: 5m

      # If an alert has successfully been sent, wait 'repeat_interval' to
      # resend them.
      repeat_interval: 3h

      # A default receiver
      receiver: team-X-mails

      # All the above attributes are inherited by all child routes and can
      # overwritten on each.

      # The child route trees.
      routes:
      # This routes performs a regular expression match on alert labels to
      # catch alerts that are related to a list of services.
      - match_re:
          service: ^(foo1|foo2|baz)$
        receiver: team-X-mails
        # The service has a sub-route for critical alerts, any alerts
        # that do not match, i.e. severity != critical, fall-back to the
        # parent node and are sent to 'team-X-mails'
        routes:
        - match:
            severity: critical
          receiver: team-X-pager
      - match:
          service: files
        receiver: team-Y-mails

        routes:
        - match:
            severity: critical
          receiver: team-Y-pager

      # This route handles all alerts coming from a database service. If there's
      # no team to handle it, it defaults to the DB team.
      - match:
          service: database
        receiver: team-DB-pager
        # Also group alerts by affected database.
        group_by: [alertname, cluster, database]
        routes:
        - match:
            owner: team-X
          receiver: team-X-pager
        - match:
            owner: team-Y
          receiver: team-Y-pager


    # Inhibition rules allow to mute a set of alerts given that another alert is
    # firing.
    # We use this to mute any warning-level notifications if the same alert is
    # already critical.
    inhibit_rules:
    - source_match:
        severity: 'critical'
      target_match:
        severity: 'warning'
      # Apply inhibition if the alertname is the same.
      equal: ['alertname', 'cluster', 'service']


    receivers:
    - name: 'team-X-mails'
      email_configs:
      - to: 'team-X+alerts@example.org'

    - name: 'team-X-pager'
      email_configs:
      - to: 'team-X+alerts-critical@example.org'
      pagerduty_configs:
      - service_key: <team-X-key>

    - name: 'team-Y-mails'
      email_configs:
      - to: 'team-Y+alerts@example.org'

    - name: 'team-Y-pager'
      pagerduty_configs:
      - service_key: <team-Y-key>

    - name: 'team-DB-pager'
      pagerduty_configs:
      - service_key: <team-DB-key>
    - name: 'team-X-hipchat'
      hipchat_configs:
      - auth_token: <auth_token>
        room_id: 85
        message_format: html
        notify: true
```


## uninstall

Remove the service, binaries, and configuration files. The data itself will be kept and needs to be removed manually, just to be on the safe side.

This is state must always be called independently.

# Testing

## Prerequisites
The tests are using test kitchen with [testinfra](http://testinfra.readthedocs.io/en/latest/) as verifier, so you need to have
- Ruby installed
- Python installed
- Vagrant installed

If you want to add tests you might want to take a look at the [documentation](http://testinfra.readthedocs.io/en/latest/modules.html#) for the modules.

## Running the test
If you run the tests for the first time you might need to run ``bundle install`` and ``pip install -r requirements.txt`` first. Afterwards you can run ``kitchen test --concurrency 6 --parallel``.
Be aware that ``--concurrency 6 --parallel`` might be a bit too demanding for some machines. You can either ommit these parameters or adjust the to fit your machines capacity.

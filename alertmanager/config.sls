# -*- coding: utf-8 -*-
# vim: ft=sls

{%- from "alertmanager/map.jinja" import alertmanager with context %}

alertmanager-rules-dir:
  file.directory:
    - name: {{ alertmanager.config_dir }}/rules
    - makedirs: True

alertmanager-targetgroups-dir:
  file.directory:
    - name: {{ alertmanager.config_dir }}/tgroups
    - makedirs: True

alertmanager-templates-dir:
  file.directory:
    - name: {{ alertmanager.config_dir }}/templates
    - makedirs: True

alertmanager-config:
  file.managed:
    - name: {{ alertmanager.config_dir }}/alertmanager.yml
    - source: salt://alertmanager/files/alertmanager.yml.j2
    - mode: 644
    - user: root
    - group: root
    - template: jinja
  service.running:
    - name: alertmanager
    - enable: True
    - onlyif: ls {{ alertmanager.config_dir }}/alertmanager.yml
  module.run:
    - name: service.reload
    - m_name: alertmanager
    - onchanges:
      - file: {{ alertmanager.config_dir }}/alertmanager.yml

# Enabling the service here to ensure each state is independent.
#alertmanager-service:
#  service.running:
#    - name: alertmanager
#    - enable: True
#  watch:
#    - file.managed: alertmanager-config

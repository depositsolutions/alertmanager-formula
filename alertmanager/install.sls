# -*- coding: utf-8 -*-
# vim: ft=sls

{% from "alertmanager/map.jinja" import alertmanager with context %}

alertmanager-create-user:
  user.present:
    - name: {{ alertmanager.service_user }}
    - shell: /bin/false
    - system: True
    - home: /dev/null
    - createhome: False

alertmanager-create-group:
  group.present:
    - name: {{ alertmanager.service_group }}
    - members:
      - {{ alertmanager.service_user }}
    - require:
      - user: {{ alertmanager.service_user }}

alertmanager-bin-dir:
  file.directory:
   - name: {{ alertmanager.bin_dir }}
   - makedirs: True

alertmanager-dist-dir:
 file.directory:
   - name: {{ alertmanager.dist_dir }}
   - makedirs: True

alertmanager-config-dir:
  file.directory:
    - name: {{ alertmanager.config_dir }}
    - makedirs: True

alertmanager-data-dir:
  file.directory:
    - name: {{ alertmanager.data_dir }}
    - makedirs: True
    - user: {{ alertmanager.service_user }}
    - group: {{ alertmanager.service_group }}

alertmanager-install-binary:
  archive.extracted:
    - name: {{ alertmanager.dist_dir }}/alertmanager-{{ alertmanager.version }}
    - source: https://github.com/prometheus/alertmanager/releases/download/v{{ alertmanager.version }}/alertmanager-{{ alertmanager.version }}.{{ grains['kernel'] | lower }}-{{ alertmanager.arch }}.tar.gz
    - source_hash: https://github.com/prometheus/alertmanager/releases/download/v{{ alertmanager.version }}/sha256sums.txt
    - options: --strip-components=1
    - user: root
    - group: root
    - enforce_toplevel: False
    - unless:
      - '[[ -f {{ alertmanager.dist_dir }}/alertmanager-{{ alertmanager.version }}/alertmanager ]]'
  file.symlink:
    - name: {{ alertmanager.bin_dir }}/alertmanager
    - target: {{ alertmanager.dist_dir }}/alertmanager-{{ alertmanager.version }}/alertmanager
    - mode: 0755
    - unless:
      - '[[ -f {{ alertmanager.bin_dir }}/alertmanager ]] && {{ alertmanager.bin_dir }}/alertmanager -v | grep {{ alertmanager.version }}'

alertmanager-install-service:
  file.managed:
    - name: /etc/systemd/system/alertmanager.service
    - source: salt://alertmanager/files/alertmanager.service.j2
    - template: jinja
    - context:
        alertmanager: {{ alertmanager }}
  module.run:
    - name: service.restart
    - m_name: alertmanager
    - onlyif: ls {{ alertmanager.config_dir }}/alertmanager.yml
    - onchanges:
      - file: /etc/systemd/system/alertmanager.service
